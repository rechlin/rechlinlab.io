---
title: for zach
---
Response to [When Your Mind is Too Full to Write](https://medium.com/still-hurting/when-your-mind-is-too-full-to-write-12e37b98d7c1) by Zach J. Payne.

> Lately, there have been too many days like today, where there are a multitude of thoughts streaking through my head.

Zach, I also have far too many thoughts spinning around in my head. I find that Mindfulness practices help me with this problem.

The books I have read, and followed, on this topic are: 
* "The Power of Now" by Eckharte Tolle
* "The Mindful Athlete" by George Mumford

I started with "Be Here Now" by Ram Dass, however, in places, I felt the attitudes toward women were unacceptable to me.

Another book that may help you is:
* "Healing Through the Dark Emotions" by Miriam Greenspan

This book talks about the emotion-phobia of our culture and how to get past it, to actually welcome and learn from your emotions.

Zach, thanks for sharing your life, and your issues, with your friends on Medium.

