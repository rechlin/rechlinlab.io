---
title: Setting up Microsoft Subsystem for Windows (WSL)
---

Raw info from work.

 Jan 13, 2020

 _Learning: setting up Microsoft Subsystem for Windows_

  - My system did not have locale setup properly.

    - Caused problems on other systems, because they expected to set up the locale to match the system I was connecting from

    - Seems to require `/etc/locale.conf`, which contains one line:

      - `LANG="en_US.UTF-8"`

      - That file did not exist

      - Copied this info from my Linux devserver

    - I have installed Ubuntu in WSL

      - I did not pay money for a "remix" version of Fedora.

    - That did not fix it.

    - Restarting something.

      - WSL?

    - Did not work

  - Found this page:

    - This answer:

      - ""... you can run this command to change the Ubuntu locale after install completes"

      - `sudo update-locale LANG=en_US.UTF8`

      - You will have to relaunch bash.exe for this change to take effect.

    - from: WSL Installation Guide for Win 10

      - https://docs.microsoft.com/en-us/windows/wsl/install-win10?redirectedfrom=MSDN#enable-the-windows-subsystem-for-linux-feature-gui

# Setting up Windows Terminal (Preview)



Jan 22, 2020

- _Learn: _Setting up Windows Terminal (Preview)_

  - Start by viewing the settings file

    - Click on the Down arrow to the right of the right-most terminal name

    - There is a short list of terminal types

    - If you have WSL installed, with one or more OS installations, they will be listed here (?)

    - Choose "Settings"

      - A simple configuration file for Windows Terminal will open in Notepad

  - Let's download the "default" settings, which you can't change

    - Click on the Down arrow to the right of the right-most terminal name

    - Click and hold the Alt key, as described in the first line of the settings file

    - Click on Settings

      - This should open another copy of Notepad with a much larger settings file

      - These are the default settings

  - **Schemes - set colors for your terminal type**

    - Info here:

      - https://github.com/microsoft/terminal/blob/master/doc/user-docs/UsingJsonSettings.md

  - **Interaction between WSL and Windows Terminal (Preview)**

    - If you have both a "commandline" and a "source"

      - You get two entries in your list of "profiles" in Terminal

    - Both of these give you "one" entry:

```

        {

            "guid": "{c6eaf9f4-32a7-5fdc-b5cf-066e8a4b1e40}",

            "hidden": false,

            "name": "Ubuntu-18.04",

            "source": "Windows.Terminal.Wsl"

        },

        {

            "guid": "{5d8306b1-749d-4bbd-8e66-2530207d387a}",

            "hidden": false,

            "name": "Dev Server",

            "commandline": "ubuntu1804 run ssh dev3-245"

        },

```

    - This one gives you "two" entries:

```

        {

            "guid": "{c6eaf9f4-32a7-5fdc-b5cf-066e8a4b1e40}",

            "hidden": false,

            "name": "Ubuntu-18.04",

            "commandline": "ubuntu1804 run ssh dev3-245"

            "source": "Windows.Terminal.Wsl"

        },

```

    - Sometimes you may find that your changes are not used

      - Example: more, different, or fewer profiles, if you are editing profiles

      - Close the settings file

      - Open it again from the SEttings menu choice

        - It will probably be different

      - This may be caused by trying to save a file that had an error

      - Errors are flagged by a message in a dialog box of Windows Terminal

  - This window defaults to Windows cmd-style cut and paste (right-click copy, right-click paste)

    - Can you change that?

