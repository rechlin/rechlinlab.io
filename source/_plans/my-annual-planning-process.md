---
title: My annual planning process
---

# Planning stuff that Shaunta does

- [Year End Planning Overview](https://medium.com/the-write-brain/how-to-spend-seven-days-setting-yourself-up-for-a-great-year-8c987b25aeab)

1. [Tools she uses](https://medium.com/the-write-brain/tools-for-a-spectacularly-productive-successful-fresh-start-in-2020-63eac93247ea)
   - Planners she uses
1. Perfect Day exercise
   - From Barbara Sher's book: "Wishcraft"
   - Describe the Perfect Day
   - Describe your usual day
   - What does it take to change one to the other?
1. New Year's Resolutions
   - Implementations of steps for moving to the perfect day
1. A Reading List
   - Books that have to do with her goals
   - Novels to read in the coming year
   - Writing Craft Books to read in the coming year
1. Meal Planning
1. January Plan
1. Write a Letter to my next-year self
   - "Just writing the letter helps me center myself for the new year. It reminds me that I’m going to end the year in a different place than I’m starting it. That whatever isn’t working right now in my life can change in the course of a year. And whatever is working is something to be proud of."

# My Plan for my plan

## Planners
- Review the planner system that Shaunta uses
- Modify it for myself, since I am not writing full time
- Print stuff for January, or not.

## Perfect Day Exercise

As described.

## Resolutions

Yup.

## Reading lists

## Meal Planning and House Planning
- recipes
- teach kids to cook
- teach kids to clean
- discuss this with Sue, even more than the other things

# Other planning points

## Set long term goals
- quote: "Most people overestimate what they can accomplish in one year, and underestimate what they can do in 10 years."

## Software is my work hobby

## Software security is interesting

## Teach my kids to take care of Linux computers


