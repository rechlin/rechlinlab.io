---
title: I Try Out Tekton Pipelines
---

Tekton pipelines is a technology for building software pipelines, in a Kubernetes environment. Your script creates and destroys containers as needed.

# My first try

I am building on my Linux desktop, locally. This means I am using Kubernetes in my workstation.

The first level of requirements for the tutorial are:
- Go
- git
- Ko
- Kubectl

Under those requirements are some more, at least in my case, building on my workstation:
- Docker-CE
- Minikube

I am running on Linux, and I don't want to use more resources than necessary. 

In order to avoid VMware or similar, I need to run Minikube with "vm-type"(SP??) =  none. 

Why? My workstation has exactly 4 cores, so all of them need to be available, at least part-time, for the cluster. With a VM, you need to put 



Minikube is the recommended way to setup on your Linux desktop. It has the following additional requirements:
- 4 vCPU nodes - which I interpret to mean 4 cores
- 8GB RAM
- 1GB swap

Since it is going to be running the containers directly in Linux, rather than in a VM, it needs to run as root, another requirement.

- Run Minikube as root

Of all those things, I have already installed Go and git.



