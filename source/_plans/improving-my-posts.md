---
title: Improving My Posts
Type: Notes to myself
---

Shaunta says there are four improvements she regularly suggests when workshopping blog posts.

* Start your post with a personal story
* End with a take-away
* Format correctly: for example, the sub-title is often not formatted correctly
* Say something unique, something that is a new take on your topic

From Medium "The Four Corrections That Will Make Your Blog Posts Shine"
by Shaunta Grimes

