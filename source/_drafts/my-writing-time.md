---
Title: Writing is fun! When will I ever be able to write?
---

# another note

Yes, I have responsibilities after work, but I can work writing into my day, if I plan my calendar.

There's bus time, which I also use for meditation, so the bus ride will be time-shared. It's short enough I don't want to do both in the same trip. The trip back home is taken up with texting home to see if anyone bought any food, like milk that I don't want to have too much of. 

And then I have to plan my food purchase. I don't want to carry too much in any trip, so I shop daily.

So, what did I learn about scheduling? I looked at my notes from Shaunta and Shannon, and other people's comments in workshops. Looks like I could start a writing calendar with time for blogging and the book every week. And time to read the rest of "The Writer's Journey". And time to kiss my wife.




# Not writing enough
Not writing everyday. Some excuse about doing household work.
- once again write some stuff every day, on the bus.
- Was doing Mindfulness meditation then
  - Meditation is helping me in all parts of my life, including writing
- May have to mix it up, and meditate every second day on the bus?
- Meditating in the morning before 6:46 alarm may work if I continue to get to bed by 10:30pm?

That's enough changes for now.