---
title: What are your spiritual roots and beliefs?
---
(fix the title later)

Or, what can I learn from you, and why should I trust you?

(yeah, fix the subtitle, too)



-----------
# other titles
- what is your spiritual path?
- what is your spiritual lineage

# outline

## What I believe:
- There is only one Goddess, but she has many faces, male and female
- After we die, stuff keeps happening, and we remember our life here on Earth
- The dead go to a place which includes many kinds of spirits, gods, goddesses, guides.
- We are not alone on Earth. We have spirits with us.
- Mindfulness is a good way to keep in touch with your inner self, and let go of past pains, inner critics, and future worries.

## What experience and training do you hame?


