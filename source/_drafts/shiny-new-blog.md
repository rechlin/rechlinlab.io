---
Title:  This is what happened when I got distracted, while writing a simple blog post.
---

_I started a bunch of blog posts before I finished any! What did I do wrong?_

Saturday, while writing a Keto blog, I realized my typos were really irritating me, and they were partly due to awful keyboards. So I wrote some notes for another blog about fixing my writing, including my typos. 

Those "notes" started getting bigger and turning into a new blog post. Worse, the info about typos was dominating the blog post, and crowding out the other writing problems. So I split the second post into a "Typos" post and a separate set of notes about my other writing problems - this blog.

And somewhere on Saturday, I bought the new keyboard. It's at work today, so I get to rediscover how awkward it is to touch type on a MacBook Air. Sigh. At least work is going to get me a new keyboard. Soon. Well, sometime soon.

I started out with one blog topic, now I had three, and I still needed to put a lot of work into the Keto post to make it work. Obviously I was never going to get anything written at this rate! 

Shaunta Grimes talks about being distracted by the "shiny, shiny" new book when you get slowed down in the first book. Here I am, distracted in the middle of a blog post! How am I ever gonna write a book?

"Calm down. Deep breaths. See how silly this problem is? First world writing problems. Loads of writers don't have enough to blog about. Like me last week. One more deep breath?" 

Yes, of course. This is just a blog post, not a book. I want to publish a blog post, any post at all, and the "Typos" post looks like it's almost written. So, Typos first. (I never thought I would say that.)

Well, I got that post written by late Sunday. It took more work than I expected, as I should have expected. I mean, software also takes more work than I expect, pretty much every time.

What was I doing wrong?

First: I wrote too much in the "notes" for the new blog topic. I think I could have put down some bullet points or titles, and got back to the Keto blog.

Second: I didn't have a set of subtitles or sections for the Keto blog post, so, no outline, no plan - hard work. I realized that in time to write a set of titles for the Typos blog post. Well, two titles, but they were a great help!

Third: Shopping while blogging? For a new keyboard? Really?

What was that other big problem, other than "shiny, shiny"? Sitting down and actually writing, daily. This weekend of blogging was probably the first time in a month that I managed to write "two days in a row", and my first writing of any kind in at least a week. I have started to deal with that - by thinking about a writing calendar. And maybe there are other steps I can take.

We'll see what happens next. For one thing, that Keto blog needs an outline.
