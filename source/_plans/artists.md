---
title: 'Marketing for Artisans and Artists'
---
Contents:
- Summary and links
- Intro for artisans and artists
- sites to post art, or create web site
  - Deviant Art
  - TheAbundantArtist.com
  - Artspan
  - FASO.com (Fine Art Show Online)
- Blogging for Artists
  - Medium post by Shaunta about blogging for writers
    - Applies to artists
  - I think the selection of topics is in the same page
- sites where you can look for subjects to blog about
  - Quora
  - Deviant Art forum
- Marketing plan for artists
  - Blogging
  - Post pictures/art online
    - watermark?
  - Participate in forums
    - Deviant Art?
    - Others?
  - Email list
    - Publish to it monthly
    - What to publish?
  - Goals for your project
  - Voice, how you wish to be seen, how to be authentically you
# Shows
- Find out all the shows that exist in Ottawa where you can sell fine art or christmas decoration
- Learn who sells there, who shops there
  - Is it a good market for your current marketing plan? 

# Where would valuable work be displayed?
## 100K plus
Not looking at those markets yet

## 20K plus
Yes, try for these markets.

Consider your other work as small models.

What size of pieces are sold at this price range?
What kind of spaces do they put it in?
What makes a piece sell in this market?

Create a plan for this market, a voice that this market will respect and look for more works from.

You are still authentically you, even in more expensive markets.
Did you have marketing classes?

Smaller pieces, that would sit in the space of a pool table or in an entrance way of a house.

What would set off an existing house?
A period house?
A new house?
A modern design?

What do people who are willing to drop $5k on a focus piece want in their house?
What will it feel like to them?
What will it mean to them?

# Pets
Rich people in Britain buy 6' by 12' paintings of their favorite hunting dogs, to hang over their fireplace. (*Sue)

Mostly, those people have houses with 12 foot ceilings, or 20 foot ceilings.

What can we do for their buddies here?
What sports and pastimes do rich people here take up?




   
