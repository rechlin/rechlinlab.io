---
title: Word-based business(es) -  New Biz idea
---

# Words with categories and with length

Use for:
- generating throw-away email addresses
- passwords
- rhyming words
- puns
- passwords etc. with over-lapping words
- API access to word information like this
- words for word-games

Options down the road:
- Include foreign words as a category (each language is a category)
- redo the web site and system for French language, and other languages
- Offer the tool set for people who want to do a similar site in another language or for a specialized set of data (medical?)

# source of data
- how to access the words in the spelling dictionary in Linux
- pay for dictionaries
- Pay English students to work on things - data entry of:
  - Get old out of copyright dictionaries from eBay
  - scan and OCR
  - Students correct stuff

# Revenue

## Ad-supported web site
- Make the ads not dominate things

## option of paying $5/year
- For customers who want to use this site a lot

## API delivery of info to other people's sites
- they can ask for the options that people want for passwords, etc. And send the datat to get a set of (5?) random generated PWs.
  - Charge for this should be low, like $5.00 for 10000 connections
  - It takes several connections to get all the data for the customer to get a password

## office systems
- when you change your password, show you this tool as an app, to generate some passwords and choose one
- related: apply check for 1000 top passwords from passwords harvested by hackers
- also checks for passwords like (company name) + number (+ special)
  - `Solace1!`

## Dictionary site that includes 
  - meanings, 
  - related words, 
  - words licensed from Urban Dictionary, 
  - etymology, 
  - license some from OED
  - Words and names from pop culture:
    - Dilbert phrases and names
    - names from books and movies

## email forwarding site:
- you get an email address (generated using the full rule set above), and all mail is forwarded to your mail box
- you can log in to site to send replies, or send reply from your email program to this site
  - we forward it with a reply-to address, that is then re-sent by our computer to the original email address.
  - you get the email address sent to you, either in the original, or as an attachment, or in a separate email message
- we re-use addresses if no email to them for a year
- we track for you all email sent to this address
  - source address
  - number of times in each (day/week/month)
- we bounce email from certain senders if you wish
  - or with certain keywords and spam




# How to promote

Promos and blogs and articles

Reference in my promos and blogs and articles:
- rules from dept of US government that sets rules
- XKCD
- Related


# The UX for the tools

- User chooses:
  - length range
  - does it need numbers, upper/lower, special chars
  - are spaces allowed?
  - they can upload special words, like local words for plants/animals in their part of world or language
  - categories!!!
  - insert special word or name first, middle, or last (their name, or NOT)
- Rule-sets
  - User can select a rule-set
  - user can save for community use a new rule-set they created
    - Context of teh rule set 
    - what program or web site

# Web URLs I want

WordBaker is main corporate site.

# Other uses

## word games based on this data
- web site of games
- app
- games web site on an SD card to plug into your Rapspberry Pi
- Instructions on how to make that work with your Router

# Related other things to build
- Routers based on open source Linux router OS
  - Games built in??
  - Dynamic DNS to show you all the other machines on your home LAN, including your Raspberry Pi
  - DDNS could be replaced by other tools that scan the network?
  - Must work with Win, Lin, Mac, Raspbian
- Raspberry Pi package manager
  - that works PROPERLY for non-geeks

