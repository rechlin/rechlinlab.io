---
title: 'Hexo, make it your own'
p: source/_draft/
tags:
---
There are a few crucial tweaks that will personalize Hexo's sample site for you. 

Once you have installed the sample site, this is how to make those changes.

# Change the title and author of the site

Hexo has at least 2 files called `_config.yml`, in different folders. For this change, edit the one in the top level folder of the site.

If your name is Rob Splendid, and your website is a history of cats, your site information at the top of this file might look like this. As you can see, the title, subtitle, description, and author are all here.

```
# Site
title: Small Cats Through the Ages
subtitle: Cats have been a thing since the pyramids were young
description: Pictures and stories from Cleopatra to today
keywords: 
author: Rob Splendid
language:
timezone:
```

This is also an example of the way information is stored. The general information for the whole site is in the top level folder, and more specific information is in lower folders, closer to the items that are being configured.

# Put in your own banner file

The banner is the picture that is displayed at the top of every page in the site.

I use [my own photos](https://www.flickr.com/photos/rob_echlin/), and you can use one of my photos, too.
Of course you can use your own or a free professional photo from a site like [Unsplash](https://unsplash.com).
If you crop your photo, it should still be at least 1200x300 pixels.
Your chosen picture should be fairly dark near the top where the menu goes. The type in that menu is small and white, and a bit transparent.

Copy your photo to `themes/landscape/source/css/images/`.

# Change the banner link

Change the value of banner-url in `themes/landscape/source/css/_variables.styl` to point to your photo.

Here is what it looks like:

```
// Header
logo-size = 40px
subtitle-size = 16px
banner-url = "images/banner.jpg"
banner-height = 300px

```
Let's assume your picture is named `cat-licking-dog.jpg`.

Change the banner-url line to this:
```
banner-url = "images/cat-licking-dog.jpg"
```
