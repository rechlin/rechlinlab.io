---
title: "Mindfulness: Should You be 'Here' or Should You be with Your Breath?"
date: 2019-11-23 23:01:20
---

![Live vines are the here and now. Dead vines are the past.](IMG_3189-16x8.5.jpg "Live and dead vines on a concrete wall")

## Am I doing this right?

I have asked myself this question many times. My spiritualism students have asked me the same thing many times, especially after a guided meditation.
<!-- more -->

## Recently, I asked myself this question again

I was on the bus, meditating, practicing paying attention to the breath. My awareness was going deeper, closer to my inner self. Then I noticed that I was not completely "Present", the breath was my main focus, not "Being here". **Am I doing this right?**

I thought of mindfulness as being focused on the here and now, not other things.

On the other hand, my favorite mindfulness books talk about breath meditation!(1) Even a single breath, taking a deep breath, feeling it slowly fill the body down to the fingers and toes and the top of your head, can be a way to enhance your connection with your inner self.

Both meditation techniques are valuable parts of my meditation process. But which should I be doing now? 

## What was my goal? 

If my goal was to achieve deep connection, then I should pay attention to the breath, let it move deeply into the body, and go to that connection. If my goal was to meditate in a way that allowed me to notice when we got to my bus stop, maybe a less intense technique would be better.

## What did I do?

I moved to a more complete to a more complete focus on the here and now, and continued to work on the deep connection. Then after a couple of minutes, I became frustrated that I was not achieving the deep connection.

I thought for a moment, covering most of the points above, and moved back to the breath meditation. However, I had lost the inspiration, the feeling, that allowed me to quickly move deeper. I was just breathing. Even sitting here writing, I am achieving a deeper connection than I had on the bus!

On the other hand, I had no trouble moving out of meditation to get off the bus!

## What did I learn?

I reminded myself, through re-reading, that being focused on the here and now, means simply this time, this place. The thing to avoid is letting the egoic mind take you on a tour of events that are not in the here and now.

For example, it might show memories of past events or fears of future events. Both of those are not now. The egoic mind might also portray things that might or might not be happening in other places at this time, such as in our children's school, our manager's office, or in the mind of other people. Those things are now, but not here.

The breath is part of the here and now, and it is OK to focus on it as a Mindfulness exercise.

As best I can remember, this is what actually happened. We all encounter unexpected things in mindfulness. This is how I dealt with this one. To me, the important thing seems to be to keep trying, to recognize when you are not in mindfulness, and get back into it.


## Footnotes

(1) "My favorite meditation books talk about breath meditation"
Eckhart Tolle, the Power of Now
- breathing meditation - Chapter 6 - "The Inner Body", Section: "Let the Breath Take You into the Body"
- p.125 in my paperback version - 2004 Paperback edition

George Mumford, The Mindful Athlete
- Awareness of Breath (AOB) - Chapter 3 - "Concentration - Focused Awareness" 
- p. 101 in my paperback version - about 9 pages about breathing meditation
