---
title: One way to Setup Raspberry Pi
---

# Obtain stuff you need

## Raspberry Pi

I was given one by a friend who was not using it, and had a newer model of Raspberry Pi.

You can also buy them online.
For example, the [Raspberry Pi web site](http://raspberrypi.org/) lists local distributors.

## An 8GB or larger SD card

You need a full size SD card, for my older Raspberry Pi.
Apparently the Raspberry Pi 4 uses the micro-SD card.

I think a good size is 16GB to 64GB.


## A way to install the OS on your SD card

Either a laptop/desktop with an SD slot, or a cardreader that will plug into a USB port.

## Power supply

Should be high quality to be able to provide full power to the Raspberry Pi.
I used a dual port plug that claims to deliver 2.4A on each of the two USB ports. This is intended for high speed charging of your phone or other device. It also means it will have no trouble delivering the 0.1A required by the Raspberry Pi. 

( TODO verify 100ma power requirements)

## Cable

Should be high quality, or short, to reduce the amount of power lost in the cable.

# Download Raspbian Linux Distribution


# Install the downloaded Linux Distro on the SD card

[Full instructions](https://www.raspberrypi.org/documentation/installation/installing-images/README.md)

Unzip the downloaded zip file.
Check what devices are found on your computer before you connect the SD card.
- On a linux or Mac command line: `lsblk -p`
- 
Connect the SD card to your computer, using builtin slot or a cardreader.

## Determine the device that is the SD card

## install the Raspbian image

## Enable ssh while the SD card is mounted on your computer

## Enable passwordless login
On a Windows computer, you may have to install Putty, or other ssh software.

### Create an ssh key pair
You may have one already.

### Copy the pub file to the SD card
It goes into home/pi/.ssh/
Rename it to `authorized_keys`
If more than one person wants to use passwordless ssh login, append each pub file to the authorized_keys file.

# Initial setup

## Plug it in and turn it on

## Find it on your network

### Install nmap

Check what IP address range your computer is using on your home LAN.
- Mac or Linux: `ifconfig`
- Windows: `ipconfig`

On a home network, you usually have the network on 192.168.0.XX or 192.168.1.XX.

### Run nmap - this may take about 5 minutes

There may be a faster version of the nmap command.

- `sudo nmap 192.168.1.1/24`

Look in the output from the above command for a 192 address.
The second last number between dots will show what address you are using.

This information assumes you have IPv4 addresses. You might have IPv6 addresses.

This is the entry for the Raspberry Pi on my system:
```
Nmap scan report for 192.168.1.18
Host is up (0.0048s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
22/tcp open  ssh
MAC Address: B8:27:EB:0F:7B:09 (Raspberry Pi Foundation)
```

The MAC address identifies it as being from the Raspberry Pi Foundation.
This simple test may not work for a newer Raspberry Pi.

Notice that port 22 (ssh) is open.
This is because we created an empty ssh file in the boot partition of the Raspberry Pi during setup.
When it started up, it noticed that file existed, and enabled ssh access, then deleted that file.

# Initial setup

Things to do when you get your pi running.

Login from another computer using ssh.
The ssh username is `pi`
If you are asked for the password, it is `raspberry`

- Change the password of the Pi account
  - Run this command: `passwd`
  - The program will ask for the current password for pi:
    - Enter existing `raspberry`
    - Enter your new password
    - Enter your new password again

# Setup MoinMoin

Using: https://moinmo.in/HowTo/Debian8

running "virutalenv /srv/moin/pythonenv as "pi"
this might be incorrect?
Should be moin user??

To fix, just `sudo chown -R moin /srv/moin` Maybe?

Tools for encryption - apache2-utils
- sudo apt install apache2-utils

# Setup Basic Authentication
