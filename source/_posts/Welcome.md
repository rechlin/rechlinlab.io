---
title: Welcome
date: 2019-08-09 09:58:20
tags: new, info
---

This site is new. Currently it has links to _some_ of my [Medium articles](/pages/Friend-Links-on-Medium). Those links are **friend** links. That means they don't count against a non-member's monthly free page view quota on Medium.


All my very best,
Rob
