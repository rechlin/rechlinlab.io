---
title: Friend Links on Medium
date: 2019-08-05 18:49:05
banner-url: /rechlin/pages/Friend-Links-on-Medium/flood-oven-tardis-stump.jpg
---

![A wood oven under a log shelter in the woods at Raven's Knoll, beside a flooded trail](flood-oven-tardis-stump.jpg)

Here are friend links for my recent stories on Medium:


# November 2019
- [How to Fix a Broken Tent Zipper. First I tried a Sarong.](https://medium.com/with-no-explanation/how-to-fix-a-broken-tent-zipper-first-i-tried-a-sarong-8b0b2b2fc8a7?sk=00ee86805a18acf3122e3666da67cd2a) Was: "When the Tent Zipper Broke"
- [I typo badly. First step - check my keyboards](https://medium.com/with-no-explanation/i-typo-badly-first-step-its-not-my-fault-4991244525a0?source=friends_link&sk=523e9986a40a8d20e0213e27fc2c4f95)
- [I get tired of clicking. Sorry guys.](https://medium.com/with-no-explanation/i-get-tired-of-clicking-sorry-guys-8643b46d5aa?source=friends_link&sk=0dfc5e591a3357980058efe12ad399a2)

# October 2019

- [Visualization sank the putt: Why didn’t I try it again?](https://medium.com/with-no-explanation/visualization-sank-the-putt-why-didnt-i-try-it-again-adffe7b3fd78?source=friends_link&sk=0f2de3a180982c393e10ec1b7257d7e1)

# September 2019
- [Hexo: Make it your own](https://medium.com/with-no-explanation/hexo-make-it-your-own-846cc450ff18?source=friends_link&sk=2de99b61f1f7012ca070070272a06920)


# August 2019
- [Green Computer Gremlins Meet Molly Human](https://medium.com/with-no-explanation/green-computer-gremlins-meet-molly-human-50f437542df9?source=friends_link&sk=d39d720cf227355f38b322e4828390bb)
- [How Not to Choose a Static Site Generator](https://medium.com/with-no-explanation/how-not-to-choose-a-static-site-generator-d3fced9e2efe?source=friends_link&sk=49cb325167387f4f7527dce960bed2ce)
- "When the Tent Zipper Broke" Republished Nov 2019 as "How to Fix a Broken Tent Zipper. First I tried a Sarong."

# July 2019
- [The Water's on Fire!](https://medium.com/with-no-explanation/the-waters-on-fire-2fe3e546ca1b?source=friends_link&sk=d9f64f32971c62292690a40a34bc5ff6)
- [The Chocolate Response](https://medium.com/with-no-explanation/the-chocolate-response-bc08370c9dd9)



