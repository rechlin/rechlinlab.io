---
title: My fave Keto bar has changed. Woe is me, what ever will I do?
---
I feel I have to make a change to my Keto diet.

I have not changed my food plan for months, but my eating has changed.
Partly, I am obviously blaming this on Nature Valley and the changes in my fave protein bar. Partly, I know I am at fault for not taking proper action to have a diet that I can continue for years.

First let's discuss Nature Valley's delicious little bar. It's a protein bar called "Coconut Almond". Of all the "relatively cheap" protein bars I know of, this is the one with the best fat:carb ratio, and a reasonable fat:protein ratio.

Definition: "Relatively cheap" - These bars were about 80 cents each when I first bought them, and they weigh about 40g each. (about 1.4 oz). I don't really want to spend $20US for 6 even tinier bars that weigh only 1 oz each (28g), even though the Fat Bars have super fat:carb ratios.

Now, let's talk about what has changed: First, me. I eat a lot more of these little bars than I did. Basically, I was losing weight and I had to do something. I was already eating one a day to top up my breakfast. Now I added two more a day to top up, and sometimes another as part of a super-fast supper.

I know that sounds like a recipe for overweight disaster, and I don't suggest you try this at home. I have a BMI of 20.8. I walked about 70K steps every week. I ain't worried about hitting 35 BMI any time soon.

However, the real problem is that I never established a large set of meals that I could cook and eat, especially on the days when I tired. I eat mostly the same thing every day. Breakfast is a set piece with 3 scrambled eggs (microwaved). I always add 2 tbsp coconut oil, and either cheese, hummus, sour cream, or cream cheese. We can't bacon in the house, it affects my wife's asthma.

Next

That bar has changed, and I don't like this change: I am afraid it means the Keto macros have changed. And I don't like the new taste quite as much.

First, Walmart's price went down, a lot, from $2.97 to $2.47 (all prices Canadian, eh? Multiple by 3/4 to get to Maircan bucks, eh?)
That was an alarm bell for me. When Walmart drops prices on something, sometimes it means the quality is also going down.

This bar had already dropped from about $3.27 a few months ago.
I noticed and paid attention, but the food did not change then.

This time, the food changed.
- The color of the "coconut flavored coating", on the bottom, changed from coconut white to a light brown.
- There seem to be fewer almonds, and the texture seems to have more of the crunch of rice crisps. 
- It might be sweeter, which is a problem, because I am now used to eating food with no sugar added.
- I don't remember any peanut ingredients before, and now it has peanut oil. Peanuts matter to me because one of my sons is mildly allergic to peanuts.

So what do you do when you are not sure about a product? 
- You write a polite email to the manufacturer. 
- You talk to friends who also eat that food.

No way, man! I have been considering making my own Keto treats for weeks now. And this is my chance! I'm  gonna make some Keto snacks I can take to work, add to my 

I just bought some ingredients to make my own Keto treats

-------------------------
Other points:

I am eating too much of this treat. The ratio of fat to net carbs is only 12g to 9g (4:3).
12g fat to 10g protein is also too low.

I am going to buy some cocoa butter and other ingredients to make my own Keto treats. I already have some lard for savoury treats.

--
old points

Their Coconut-Almond protein bar has become a mainstay of extra calories for me. I know that aa lot of Keto eaters are trying to lose weight, and don't need extra calories, but I am not. I am eating foods that I expect to help my gut bacteria. That means "low carb" which is close to Keto, plus other things the gut bacteria like, such as fish oil, electrolytes, and a variety  of .

The cook in our family buys some of the supper ingredients on the way home and them cooks supper. And I am the cook, so I get tired some days and take shortcuts. So the meals can be a bit light. They normally have lots of fresh baby tomatoes, cucumber slices, ripe bell pepper slices, and maybe some cauliflower or 

