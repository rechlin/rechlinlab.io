---
Title: I write badly. These 3 tools and tactics help.
---

The problems: Typos, not finishing, not actually writing.

Typos are irritating, and they seem to be completely under my own control.

It's frustrating to rattle off a line of prose, or a line of code, then look at the screen and discover there are 3 fewer spaces than there needs to be, twice an adjacent letter got hit instead, and a couple times the right hand key was hit before the left hand key that was supposed to come first, or the other way around.

It's worse on the work computer and on my shiny Mac laptop, not so bad on the computer in the kitchen. 

Why? Keyboards. 

- The computer at work has an $8 keyboard that IT found for me when I complained that my original keyboard had the labels worn off of several letters.
- The Macbook Air has an astonishingly useless keyboard for such an expensive toy.
- The computer in the kitchen, the one that ain't bad?, has a 5+ year old keyboard that came with a corporate grade Dell computer that I got second hand. Dell used to provide their corporate customers with decent keyboards. And now it's the best keyboard I have.

Well, actually I set up my writing space with a couple of enhancements on Saturday. First, when I got that ultra-cheap keyboard from IT, they actually agreed to buy a new keyboard if I picked one out. And I decided I wanted to have the same keyboard at work as at home.

So on Saturday, I went to the local Canda Computers store that has about 15 or 25 computer keyboards on display for people to try. I know that work is as cheap as I am, so I ignored the backlit gamer boards with extra Gamer Macro Keys and, well, some other things that the sales per said and I ignored. Gotta ask my son why all that stuff? That shit starts at $169 and climbs straight past $239 to prices that I ignored completely.

So there was one keyboard with only keyboard features, that doesn't make as much clackity-clack as some others. Quiet enough for home or cube-space. It still cost $100, and I think work will spring for one. I took one home.


------

# I have a huge number of typos. 
Physical writing environment

I am taking steps to fix up my physical writing environment.
I just added a booster seat to the kitchen chair. Well, that wooden thing is actually a ring toss game that my daughter made in elementary school - upside down.
<pic>
---
# Points to make

