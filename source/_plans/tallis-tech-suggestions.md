---
title: Tallis suggests learning these
---

# Technology stack for the build automation plan
- Tekton.dev - part of Jenkins-X
- Nexus WebHooks
- Lambda (AWS)
- Dynamo DB (AWS)
- API Gateway (AWS)
- Git repos for everything, not sure what


# Start by downloading it and following a tutorial
   - Install from https://github.com/tektoncd/cli/
   - Tutorial: https://github.com/tektoncd/pipeline/blob/master/docs/tutorial.md
   - Requirements:
     - Kubernetes?
- Next steps:
   - Install bash for windows, I mean Windows System for Linux
   - Set up a cluster to test with, on AWS.
   - Instructions: https://ramhiser.com/post/2018-05-20-setting-up-a-kubernetes-cluster-on-aws-in-5-minutes/