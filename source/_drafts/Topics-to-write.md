---
Title: Topics I could write
---

## Refocus my career
_2019-11-19_
Refocus my writing career plans.
- Write fiction books, same "computer people" book as before
  - This will definitely be a good process for another income stream
- Blogs:
  - Computer topics have done best for me
  - I am able to get them curated, sometimes
  - So write those
- Other topic areas
  - Keto
  - Mindfulness
- Other income streams
  - Healing book and web site
  - "Hosting some Community web sites" toolkit
    - Formerly called Groups
    - Also useful for support and specs and other company plans

This is a list of topics in each of some categories

# Software Topic areas:
- Tekton
- Kubernetes
  - or PKS - "Pivotal Container Services" packaging of it?
- Ansible
- Coverity
- Jenkins
- AMQP
- MQTT
- Solace 

# Software Blog Titles

- Text editors for software developers
- Text editors for non-software developers
- Use cases for VIM
  - Front end dev wants to do back end or has to confiugre a Linux system
- Alternatives for VIM in command line environments
  - VSCode - link from Mac or Windows to do the actual editing
  - GUI editors
  - CLI editors
  - Emacs
- Emacs
- Starting to maintain a Linux device
  - Use cases?
  - what to know for each use case
- When your screen goes wonky in Linux or has died
  - Get to command line
  - Switch to CLI and back
  - Log in from another computer
    - Must have ssh server running
- Ask Zach for topics that he would like to be able to refer Shaunta to
  - Already asked him on Slack - Ninja Writers Academy
- Techie jobs are creative, but not for paper artists
  - Techie jobs are rarely about not talking to anyone else, very social
  - Parents of techie kids, do not despair. There be creativity here, too. And your kids are well positioned to make that work for them, from your artistic example.


# Low Carb
- I am not really doing Keto. Let me tell you how my diet is better, at least for me
- Keto snack balls
- Keto burgers
- Higher quality fast meals
- I don't prepare dishes. I just eat ingredients. This is how I get that to work for me.



# meditation
- Meditating on a bus
- Eyes close or open? Choices in Mindfulness
- Breathing: Being here or being with the breath?
- Remembering to be here and now. Remembering my connection to my inner self. Why is it so hard?


# Outlines

## Mindfulness: Being here or being with the breath?

points
- Being with the breath is a good meditation technique (details)
- Being here and now is the goal of mindfulness
- A higher goal of mindfulness is to be in contact with your deeper self, your quiet self, to help you be at your best in all your decisions in daily life
- being here and now means being focused on what is happening here and now, not necessarily connecting with what is going on around you. You can be deeply focused on breathing, as long as you are focused on "now", not on how you failed or succeeded at this last week or wwould like to be able to do it in the future, or how you might fail in the future.

Opening?
I was on the bus, practising meditation, practising paying attention to the breath when I noticed that I was not completely here, the breath was my main focus.

Conclusion
- A good way to get into a meditation is to use the breath
- You can do a meditation that is just about focusing on the breath
- If you can get in touch with your inner quiet self, and hold that link as a background to daily life, use the breath, use other tools, use everything you know to keep that link. From my experience, and some comments from the books I have, it's normal to lose that connection, keep trying.