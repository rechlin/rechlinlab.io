---
title: "Visualization sank the putt: Why didn’t I try it again?"
date: 2019-10-13 19:20:55
tags:
---

*Was I afraid it wouldn't work again?*

It was at the company golf tournament. It's always played "best ball" style, where each group of four golfers work together. They all from where the best shot landed each time. The game goes faster, and it means people who never golf are more likely to try it. 

I only golf once a year, so I was hoping to maybe have my shot used once or twice in the day.

It was an 8 foot putt on about our 4th hole that it happened. <!-- more -->Two of the other players had tried before me, from the same spot as I was going to try from - because you always shoot from the same spot in "best ball". I saw how hard they hit the ball, and where it was aimed, and how much it turned downhill.

I decided to visualize my ball going toward the hole from my practice swings beside my golf ball. I saw what my swing did and took my best guess at where the ball would go. That was a miss, so I did another practice swing. That one appeared to be going closer. I tried a third time and it appeared to go in. I tried again, as much like the third as possible, and the visualization went in again. 

So I stepped forward a few inches, so my club would hit the ball with the same swing as in my practise. I swung the same as the 3rd and 4th practice strokes, as far as I could tell. The ball went gently out toward the hole, turning sideways a bit at the end, and dropped in the hole!

We were all happy and excited. And we were all amazed that I was able to sink a putt. I explained what I had tried to do, and everyone nodded as if they understood. Then the last player of the group tried the putt, and missed. So I had saved the group a whole stroke on that hole!

But I never tried to use visualization on any of the remaining holes, not on a putt, not on any of the other shots. It worked!, so why didn't I do it again?

No one commented on it when I said what I had done, so maybe that intimidated me? Maybe I just forgot to try it again? That doesn't make sense. I don't have a good answer here, so your answer in the comments is probably useful.

I feel like there is something I can learn from this experience. What does this visualization success mean in my larger life? Can I apply visualization directly or is visualization symbolic of other things I can apply in my life?

If I take the visualization technique and apply it directly in at home, in my writing, and in my work, how would that work?

For example, I could visualize myself getting up in the morning at 6am, going to sleep at night at 11pm, or 10:30pm. Those are both things I want to do. I could visualize all the steps of going to bed, getting ready and being in bed at a given time, getting up at the right time, not being distracted through the morning preps for work, and writing for half an hour. Being on the bus at a given time, and at work by 8:15am.

This is a long process with many steps. It also has interactions with other people along the way. It is usually those interactions that make me late to bed at night. I am not sure visualization is the right tool for this process. This is an area for establishing habits with tools like a diary app and for talking with family to set expectations. These are both tools that I am using in this area.

In other words, visualization is more likely a symbol of other tools that I can use for this part of my life.

I will consider what other tools might be appropriate for improving my work, my sleep, and my writing. I will also consider using visualization for keeping my long term goals top of mind. Perhaps for my writing, I can visualize a mid term goal such as achieving 12 blog entries every 4 weeks.

Do you use visualization in your life? Are there other mental tools you use, or could use, to help yourself achieve your goals? Please share in the comments.
