---
title: UTF-8 in Linux
---

# Setting up UTF-8 in Linux, including Ubuntu in Windows Subsystem for Linux


# Windows Subsystem for linux

Story about all the work I put in trying to find this out.
Some info seemed to say "UTF8" (no dash) was correct.



# WSL Installation Guide for Win 10

- https://docs.microsoft.com/en-us/windows/wsl/install-win10?redirectedfrom=MSDN#enable-the-windows-subsystem-for-linux-feature-gui

# Locale is likely not set up properly

To check, enter this command:

- `locale`

- Expected result:

"""

LANG=en_US.UTF-8

LC_CTYPE="en_US.UTF-8"

LC_NUMERIC="en_US.UTF-8"

LC_TIME="en_US.UTF-8"

LC_COLLATE="en_US.UTF-8"

LC_MONETARY="en_US.UTF-8"

LC_MESSAGES="en_US.UTF-8"

LC_PAPER="en_US.UTF-8"

LC_NAME="en_US.UTF-8"

LC_ADDRESS="en_US.UTF-8"

LC_TELEPHONE="en_US.UTF-8"

LC_MEASUREMENT="en_US.UTF-8"

LC_IDENTIFICATION="en_US.UTF-8"

LC_ALL=

"""

- Error result:

"""

LANG=C.UTF-8

LANGUAGE=

LC_CTYPE="C.UTF-8"

LC_NUMERIC="C.UTF-8"

LC_TIME="C.UTF-8"

LC_COLLATE="C.UTF-8"

LC_MONETARY="C.UTF-8"

LC_MESSAGES="C.UTF-8"

LC_PAPER="C.UTF-8"

LC_NAME="C.UTF-8"

LC_ADDRESS="C.UTF-8"

LC_TELEPHONE="C.UTF-8"

LC_MEASUREMENT="C.UTF-8"

LC_IDENTIFICATION="C.UTF-8"

LC_ALL=

"""

- Run this command to change the Ubuntu locale to en_US

- `sudo update-locale LANG=en_US.UTF-8`

- Relaunch bash.exe for this change to take effect.


Need UTF-8 set up for Mosh.
Need Mosh for connecting to Mac Mini (pseudo-servers) over flaky connections.

The flaky connections to the mac minis MIGHT be due to the behavior of the mac. When no one is using it, they go into a low energy mode where they listen for network connections and wake up (slowly) when they get one, in time for a subsequent connection.
Maybe they listen for 2 taps, close together?

